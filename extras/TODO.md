# ToDo


## Core Library

- Add `files` field to `package%`.
- stricter `*DEPEND`,
  pkgs have to match `CATEGORY/PN`,
  implement `!CATEGORY/PN` and `!!CATEGORY/PN`,
  also rewrite `constrint-flag`,
  `(quote DEPEND)` -> `"${DEPEND}"` (also for `constrint-flag`).


## Tools

- Do not include `racket-ebuild` (dispatcher) in it's own listing (`--list`)?


## Removals and replacements

- Replace `sh-function` for `at-exp` or `pprint-compact`?


## Structure

- Move `private/*` files up in directory hierarchy, making them public
  and move test modules from them to files inside `tests` directory?


## Classes vs Objects

- Migrate metadata structs (`longdescription`, `maintainer`, etc.) to classes?


## Templates

- Swap wording `ebuild-git%` -> `git-ebuild%`?


## Collectors

- Add new sub-package `collectors` with simple client for package servers,
  like the Collector2 program.
