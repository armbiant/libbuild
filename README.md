# Racket-Ebuild

<p align="center">
    <a href="http://pkgs.racket-lang.org/package/ebuild">
        <img src="./extras/badges/raco_pkg_install-ebuild-aa00ff.svg">
    </a>
    <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/gentoo-racket/racket-ebuild">
        <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/gentoo-racket/racket-ebuild/">
    </a>
    <a href="https://gitlab.com/gentoo-racket/racket-ebuild/pipelines">
        <img src="https://gitlab.com/gentoo-racket/racket-ebuild/badges/master/pipeline.svg">
    </a>
</p>

Library to ease ebuild creation.

<p align="center">
    <img src="logo.png" width="250" height="250">
</p>


## About

This package is meant to help [Gentoo](https://gentoo.org/)
(and it's forks) developers
in creating [ebuilds](https://wiki.gentoo.org/wiki/Ebuild).

It is primarily made with
[collector2](https://gitlab.com/gentoo-racket/racket-collector2) in mind.


### Online Documentation

You can read more documentation on
[GitLab pages](https://gentoo-racket.gitlab.io/racket-ebuild/).


### Subpackages

| name                | role                                                |
|---------------------|-----------------------------------------------------|
| ebuild              | metapackage, pulls in all of the components         |
| ebuild-doc          | documentation of the whole project                  |
| ebuild-lib          | core library                                        |
| ebuild-templates    | additional ebuild templates (classes & mixins)      |
| ebuild-test         | tests of exported APIs and tools                    |
| ebuild-tools        | command-line programs using ebuild-lib              |
| ebuild-transformers | helpers transforming miscellaneous data for ebuilds |


### Installation

#### From Packages Catalog

Requesting `ebuild` installation should pull in all subpackages.
```sh
raco pkg install ebuild
```

#### From repository

```sh
make install
```

or install individual packages, for example to install "ebuild-lib":
```sh
( cd src/ebuild-lib && raco pkg install --no-docs --skip-installed )
```


## Dependencies

Packages without links should already be in your version of main Racket distribution.

| name                                                                | for                 |
|---------------------------------------------------------------------|---------------------|
| [reprovide-lang-lib](https://github.com/AlexKnauth/reprovide-lang/) | core                |
| [threading-lib](https://github.com/lexi-lambda/threading/)          | core                |
| [upi-lib](https://gitlab.com/xgqt/racket-upi/)                      | transformers,tools  |
| [ziptie-git](https://gitlab.com/xgqt/racket-ziptie/)                | tools,documentation |
| base                                                                | all                 |
| racket-doc                                                          | documentation       |
| rackunit-lib                                                        | tests               |
| rackunit-typed                                                      | tests               |
| scribble-lib                                                        | documentation       |
| typed-racket-lib                                                    | core                |

To be less verbose, if "core" is listed as requirement for a dependency,
then that dependency will appear in
[ebuild-lib](./src/ebuild-lib/info.rkt)
and maybe zero or more other packages depending on it.

Additionally, while running tests, availability of either
[pkgcheck](https://github.com/pkgcore/pkgcheck/) or
[repoman](https://gitweb.gentoo.org/proj/portage.git/tree/repoman/)
is checked, the first one found triggers the
["repoman" test](./src/ebuild-test/ebuild/tests/integration/repository/repoman.rkt)
in the test suite.


## License

### Code

Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later

### Assets

The "racket-ebuild-logo.svg" is licensed under the CC-BY-SA/2.5 license.
