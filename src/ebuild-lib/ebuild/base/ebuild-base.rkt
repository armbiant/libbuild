;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang typed/racket/base

(require typed/racket/class
         (only-in "../private/ebuild/constraint-flag.rkt" CFlag)
         (only-in "../private/ebuild/src-uri.rkt" SRC-URI))

(provide (all-defined-out))


(define ebuild-base%
  (class object%
    (init [year                     : Exact-Nonnegative-Integer]
          [EAPI                     : Exact-Nonnegative-Integer]
          [custom                   : (Listof (U (-> Any) String))]
          [inherits                 : (Listof String)]
          [DESCRIPTION              : String]
          [HOMEPAGE                 : String]
          [SRC_URI                  : (Listof SRC-URI)]
          [S                        : (Option String)]
          [LICENSE                  : String]
          [SLOT                     : String]
          [KEYWORDS                 : (Listof String)]
          [IUSE                     : (Listof String)]
          [REQUIRED_USE             : (Listof CFlag)]
          [RESTRICT                 : (Listof (U CFlag String))]
          [COMMON_DEPEND            : (Listof (U CFlag String))]
          [RDEPEND                  : (Listof (U CFlag String))]
          [DEPEND                   : (Listof (U CFlag String))]
          [BDEPEND                  : (Listof (U CFlag String))]
          [IDEPEND                  : (Listof (U CFlag String))]
          [PDEPEND                  : (Listof (U CFlag String))]
          [DOCS                     : (Listof String)]
          [PATCHES                  : (Listof String)]
          [QA_PREBUILT              : (Listof String)]
          [QA_TEXTRELS              : (Listof String)]
          [QA_EXECSTACK             : (Listof String)]
          [QA_WX_LOAD               : (Listof String)]
          [QA_FLAGS_IGNORED         : (Listof String)]
          [QA_MULTILIB_PATHS        : (Listof String)]
          [QA_PRESTRIPPED           : (Listof String)]
          [QA_SONAME                : (Listof String)]
          [QA_SONAME_NO_SYMLINK     : (Listof String)]
          [QA_AM_MAINTAINER_MODE    : (Listof String)]
          [QA_CONFIGURE_OPTIONS     : (Listof String)]
          [QA_DT_NEEDED             : (Listof String)]
          [QA_DESKTOP_FILE          : (Listof String)]
          [body                     : (Listof (U (-> Any) String))])
    (super-new)))
