;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require (only-in racket/string string-join)
         threading
         "constraint-flag.rkt"
         "variable.rkt")

(provide (all-defined-out))


(define-syntax-rule (unroll-list lst-id)
  (if (null? lst-id)
      #f
      (as-variable (symbol->string 'lst-id) (string-join lst-id))))

(define-syntax-rule (unroll-array lst-id)
  (if (null? lst-id)
      #f
      (list-as-array-variable (symbol->string 'lst-id) lst-id)))

(define-syntax-rule (unroll-DEPEND lst-id)
  (if (null? lst-id)
      #f
      (~> lst-id
          (map (lambda (v) (if (cflag? v) (cflag->string v 2) v)) _)
          (string-join _ "\n\t")
          (as-variable (symbol->string 'lst-id) _))))
