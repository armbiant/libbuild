;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require (for-syntax racket/base
                     syntax/parse
                     threading)
         (only-in racket/class generic send-generic))

(provide (all-defined-out))


(begin-for-syntax
  (define-syntax-rule (format-datum datum-format stx)
    (~>> stx
         syntax->datum
         (format datum-format)
         string->symbol
         (datum->syntax stx))))

(define-syntax (define-generic-method stx)
  (syntax-parse stx
    [(_ class-id method-id)
     (with-syntax ([generated-generic-identifier
                    (format-datum "generic-ebuild-~a" #'method-id)]
                   [generated-syntax-identifier
                    (format-datum "ebuild-~a" #'method-id)])
       #'(begin
           (define generated-generic-identifier
             (generic class-id method-id))
           (define-syntax-rule (generated-syntax-identifier id obj v)
             (send-generic obj generated-generic-identifier 'id v))
           (provide generated-generic-identifier
                    generated-syntax-identifier)))]))
