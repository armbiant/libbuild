;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


;; `make-variable' macro was also released under the MIT license,
;; so you may use THAT (!) code in your code under the terms of MIT license.
;; Referenced release: https://github.com/syntax-objects/Summer2021/issues/8

;; make shell set variable
;; (define SHELL "sh")
;; (make-variable SHELL)  =>  "SHELL=\"sh\""


#lang racket/base

(require (for-syntax racket/base syntax/parse)
         "variable.rkt")

(provide (all-defined-out))


(define-syntax (make-variable stx)
  (syntax-parse stx
    [(_)
     #'"var=var"]  ; some placeholder
    [(_ name:id)
     #'(as-variable (symbol->string 'name) name)]
    [(_ str:string)
     #'(as-variable str str)]
    [(_ ((~literal quote) sym))
     #'(as-variable (symbol->string 'sym) 'sym)]))


(define-syntax (make-array-variable stx)
  (syntax-parse stx
    [(_)
     #'"var=()"]  ; some placeholder
    [(_ name:id)
     #'(list-as-array-variable (symbol->string 'name) name)]
    [(_ str:string)
     #'(as-array-variable str str)]
    [(_ ((~literal quote) sym))
     #'(as-array-variable (symbol->string 'sym) 'sym)]))
