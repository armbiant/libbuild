;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang typed/racket/base

(require "element.rkt")

(provide (all-defined-out))


(struct slot
  ([name  : String]
   [data  : String])
  #:guard
  (lambda (name data struct-name)
    (when (equal? name "")
      (error struct-name "Given a empty string to name field"))
    (when (regexp-match #rx".* .*" name)
      (error struct-name "Given a string that contains spaces to name field"))
    (values name data))
  #:mutable
  #:transparent
  #:type-name Slot)

(struct slots
  ([lang      : (Option (U String Symbol))]
   [slotlist  : (Option (Listof Slot))]
   [subslots  : (Option String)])
  #:guard
  (lambda (lang slotlist subslots struct-name)
    (when (or (equal? lang "") (equal? lang '||))
      (error struct-name "Given a empty string to lang field"))
    (values lang slotlist subslots))
  #:mutable
  #:transparent
  #:type-name Slots)


(define (slots->xexpr [ss : Slots]) : (Listof Any)
  `(slots ,(full->element (slots-lang ss) 'lang)
          ,@(cond
              [(slots-slotlist ss)
               => (lambda (sl)
                    (map (lambda ([s : Slot])
                           `(slot ((name ,(slot-name s)))  ; attribute
                                  ,(slot-data s)           ; data
                                  ))
                         sl))]
              [else '()])
          ,@(full->element (slots-subslots ss) 'subslots)))
