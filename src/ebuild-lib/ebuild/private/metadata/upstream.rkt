;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang typed/racket/base

(require "element.rkt"
         "localized.rkt")

(provide (all-defined-out))


(struct upstreammaintainer
  ([status  : (Option (U 'active 'inactive 'unknown))]
   [email   : (Option String)]
   [name    : String])
  #:mutable
  #:transparent
  #:type-name Upstreammaintainer)

(define (upstreammaintainer->xexpr [upstreammaintainer : Upstreammaintainer])
        : (Listof Any)
  `(maintainer
    ,(let ([status (upstreammaintainer-status upstreammaintainer)])
       (cond
         [status
          `((status ,(symbol->string status)))]
         [else
          '()]))
    ,(let ([email (upstreammaintainer-email upstreammaintainer)])
       (cond
         [email
          `(email ,email)]
         [else
          '()]))
    (name ,(upstreammaintainer-name upstreammaintainer))))


;; List of all remote-id types:
;; https://wiki.gentoo.org/wiki/Project:Quality_Assurance/Upstream_remote-id_types

(define-type Remote-Id-Type
  (U 'bitbucket
     'cpan 'cpan-module 'cpe 'cran 'ctan
     'freedesktop-gitlab
     'gentoo 'github 'gitlab 'gnome-gitlab 'google-code
     'hackage 'heptapod
     'launchpad
     'osdn
     'pear 'pecl 'pypi
     'rubygems
     'savannah 'savannah-nongnu 'sourceforge 'sourcehut
     'vim))

(struct remote-id
  ([type     : Remote-Id-Type]
   [tracker  : String])
  #:mutable
  #:transparent
  #:type-name Remote-Id)

(struct doc localized ()
  #:mutable
  #:transparent
  #:type-name Doc)

(struct upstream
  ([maintainers  : (Listof Upstreammaintainer)]
   [changelog    : (Option String)]  ; CONSIDER: or URL?
   [doc          : (Option (U (Listof Doc) String))]  ; CONSIDER: or URL?
   [bugs-to      : (Option String)]  ; CONSIDER: or URL?
   [remote-ids   : (Listof Remote-Id)])
  #:mutable
  #:transparent
  #:type-name Upstream)


(define (docs->xexpr [v : (Option (U (Listof Doc) String))]) : (Listof Any)
  (cond
    [(list? v)  (map (lambda ([d : Doc]) (localized->xexpr d 'doc)) v)]
    [(string? v)  (full->element v 'doc)]
    [else  '()]))

(define (upstream->xexpr [up : Upstream]) : (Listof Any)
  `(upstream
    ,@(map upstreammaintainer->xexpr (upstream-maintainers up))
    ,@(full->element (upstream-changelog up) 'changelog)
    ,@(docs->xexpr   (upstream-doc       up))
    ,@(full->element (upstream-bugs-to   up) 'bugs-to)
    ,@(map (lambda ([ri : Remote-Id])
             `(remote-id
               ((type ,(symbol->string (remote-id-type ri))))
               ,(remote-id-tracker ri)))
           (upstream-remote-ids up))))
