;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang typed/racket/base

(require (only-in racket/string string-join string-replace))

(provide (all-defined-out))


(struct sh-function
  ([name  : String]
   [body  : String])
  #:type-name Sh-Function)


(define (sh-function->string [sf : Sh-Function]) : String
  (format "~a() {\n~a\n}" (sh-function-name sf) (sh-function-body sf)))

(define (unroll-sh-functions [lst : (Listof Sh-Function)]) : String
  (string-join (map sh-function->string lst) "\n\n"))

(: make-script (->* () (#:indent Integer) #:rest String String))

(define (make-script #:indent [indent 1] . strs)
  (let ([step (make-string indent #\tab)])
    (string-join
     ;; Correct indent level
     (map (lambda ([s : String])
            (regexp-replace "\n\t" s "\n\t\t")) strs)
     (string-append "\n" step)
     #:before-first step)))
