#lang info


(define pkg-desc "Library to ease ebuild creation. Tests.")

(define version "16.1.0")

(define pkg-authors '(xgqt))

(define license 'GPL-2.0-or-later)

(define collection 'multi)

(define deps
  '("base"))

(define build-deps
  '("rackunit-lib"
    "threading-lib"
    "upi-lib"
    "uuid"
    "ebuild-lib"
    "ebuild-modify-lang"
    "ebuild-templates"
    "ebuild-tools"
    "ebuild-transformers"))
