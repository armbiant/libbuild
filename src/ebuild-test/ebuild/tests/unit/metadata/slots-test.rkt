;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base


(module+ test
  (require rackunit
           xml
           ebuild/metadata)

  (define-values (c0 c1 c2 c3 c4 c5)
    (values
     (slots #f #f #f)
     (slots "en" '() #f)
     (slots "en" #f "ABI")
     (slots "en" (list (slot "1" "lib.so.1")) #f)
     (slots "en" (list (slot "1" "lib.so.1") (slot "2" "lib.so.2")) #f)
     (slots "en" (list (slot "1" "lib.so.1") (slot "2" "lib.so.2")) "ABI")))
  (define lc
    (list c0 c1 c2 c3 c4 c5))

  (for ([c lc])
    (check-not-false (xexpr->xml (slots->xexpr c)))))
