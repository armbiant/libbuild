;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base


(module+ test
  (require rackunit
           net/url
           ebuild/transformers/githost)

  (define c
    (url "https" #f "asd.com" #f #t
         (list (path/param "source" '())
               (path/param "asd-project" '())
               (path/param "asd.git" '()))
         '() #f))

  (define cs
    (url->string c))

  (define cg
    (githost "asd.com" "source/asd-project/asd"))

  (check-equal? cs "https://asd.com/source/asd-project/asd.git")
  (check-equal? cs (string-append (githost->string cg) ".git"))
  (check-not-false cs))
