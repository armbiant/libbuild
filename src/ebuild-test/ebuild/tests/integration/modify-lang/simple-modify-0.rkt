#lang ebuild/modify


(defs
  (define l
    '("a/a"))
  (define R
    l))

(mods
 (package ("app-misc" "unknown")
          ("9999"
           [RDEPEND R])
          (metadata
           [stabilize-allarches #false]))
 (package ("app-misc" #rx"unknown.*")
          (#rx".*"
           [RDEPEND R]
           [BDEPEND '()])))
