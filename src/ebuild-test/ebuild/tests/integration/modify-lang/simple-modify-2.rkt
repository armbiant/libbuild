#lang ebuild/modify


(defs
  (define R
    '("a/a" "b/b" "c/c")))

(mods
 (package ("app-misc" "unknown")
          (#rx".*"
           [RDEPEND R])))
