;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#|
<temp-dir>/racket/racket-ebuild/test/repository/repoman/
├── app-misc/
│   └── test-pkg/
│       ├── metadata.xml
│       └── test-package-9999.ebuild
├── metadata/
│   └── layout.conf
└── profiles/
    └── repo_name
|#


#lang ebuild

(require
 rackunit
 (prefix-in file: (only-in racket/file delete-directory/files make-directory*))
 (prefix-in system: (only-in racket/system system))
 (prefix-in uuid: (only-in uuid uuid-string)))

(provide (all-defined-out))


(define repoman-test-top-dir
  (build-path (find-system-path 'temp-dir) "racket" "racket-ebuild"))

(define repoman-test-cache-dir
  (path->string (build-path repoman-test-top-dir "cache")))

(define repoman-test-base-dir
  (build-path repoman-test-top-dir "test" "repository"))


(define (repoman-test test-name repository-object)
  (let* ([test-dir
          (path->string (build-path repoman-test-base-dir (uuid:uuid-string)))]
         [test-command
          (cond
            [(find-executable-path "pkgcheck")
             ;; Create "cache-dir" manually just in case
             (file:make-directory* repoman-test-cache-dir)
             (format "pkgcheck scan --cache-dir ~a --exit error,warning,style --verbose ~a"
                      repoman-test-cache-dir test-dir)]
            [(find-executable-path "repoman")
             "repoman -Idx full"]
            [else #false])])
    (cond
      [test-command
       (printf "Running a repoman test ~v in directory ~v~%"
               test-name test-dir)
       (file:make-directory* test-dir)
       (parameterize ([current-directory test-dir])
         (around
          (send repository-object save test-dir)
          (test-not-false test-name
                          (if test-command (system:system test-command) #t))
          (file:delete-directory/files test-dir)))]
      [else
       (displayln "[WARNING] Neither pkgcheck nor repoman were found!")
       (printf "[WARNING] Skipping test ~v~%" test-name)])
    (when (and (directory-exists? repoman-test-base-dir)
               (null? (directory-list repoman-test-base-dir)))
      (delete-directory repoman-test-base-dir))))

(define (repoman-test-metadata test-name metadata-object)
  (let* ([test-pkg
          (new package% [metadata metadata-object])]
         [test-repository
          (new repository% [name "test-repo"] [packages (list test-pkg)])])
    (repoman-test test-name test-repository)))
