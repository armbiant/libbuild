;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base


(module+ test
  (require rackunit
           racket/class
           ebuild/ebuild)

  (define Z
    (new ebuild%
         [DESCRIPTION "z package"]
         [HOMEPAGE "https://example.com/z"]
         [SRC_URI (list (src-uri #false
                                 "https://example.com/z/${PN}.tar.gz"
                                 "${PN}.tar.gz"))]
         [LICENSE "MIT"]))
  (define Z2
    (new ebuild%
         [inherits '("xdg" "desktop")]
         [DESCRIPTION "z package"]
         [HOMEPAGE "https://example.com/z"]
         [SRC_URI (list (src-uri #false
                                 "https://example.com/z/${PN}-v${PV}.tar.gz"
                                 "${P}.tar.gz")
                        (src-uri #false
                                 "https://example.com/z/assets.tar.gz"
                                 "${PN}-assets.tar.gz"))]
         [LICENSE "MIT"]
         [IUSE '("X" "cli" "gnutls" "libressl")]
         [REQUIRED_USE `(,(cflag "||" '("X" "cli"))
                         ,(cflag "^^" '("gnutls" "libressl")))]
         [RESTRICT '("mirror" "test")]
         [DEPEND '("app-test/z" "dev-libs/nss")]))


  (check-equal? (get-field SLOT Z) "0")
  (check-equal? (src-uri-name (car (get-field SRC_URI Z))) "${PN}.tar.gz")
  (check-true   (list?   (send Z2 create-list)))
  (check-true   (string? (send Z2 create))))
