;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang ebuild


(module+ test
  (require rackunit
           (only-in racket/file file->string)
           (only-in racket/port with-output-to-string)
           ebuild/templates/cargo
           ebuild/templates/gh
           ebuild/templates/git)

  (ebuild-auto-restrict? #false)

  (define frankensteins-moster%
    (ebuild-cargo-mixin (ebuild-gh-mixin (ebuild-git-mixin ebuild%))))

  (define junjis-monster
    (new frankensteins-moster%
         [year 2021]
         [EGIT_REPO_URI "zzz"]
         [GH_REPO "xxx"]
         [CRATES '("ccc")]
         [DESCRIPTION "Package for frankenstein test"]
         [KEYWORDS '("~amd64")]))

  (check-true  (class? frankensteins-moster%))

  (check-true  (subclass? frankensteins-moster% ebuild%))

  (check-false (subclass? frankensteins-moster% ebuild-cargo%))

  (check-true  (object? junjis-monster))

  (check-true  (is-a? junjis-monster frankensteins-moster%))

  (check-equal? (with-output-to-string
                  (lambda () (displayln junjis-monster)))
                (file->string "monster.ebuild")))
