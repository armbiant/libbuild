# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

EGIT_REPO_URI="zzz"

GH_DOM="gitlab.com"
GH_REPO="xxx"

CRATES="
	ccc"

inherit git-r3 gh cargo

DESCRIPTION="Package for frankenstein test"
HOMEPAGE="https://wiki.gentoo.org/wiki/No_homepage"
SRC_URI="$(cargo_crate_uris ${CRATES})"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64"
