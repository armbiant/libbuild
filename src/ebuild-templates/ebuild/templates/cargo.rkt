;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang ebuild

(require racket/contract
         (only-in racket/string string-join))

(provide (contract-out
          [ebuild-cargo-mixin
           (-> (subclass?/c ebuild-base%) any)]
          [ebuild-cargo%
           (class/c (init-field [CRATES (listof string?)]))]))


(define ebuild-cargo-mixin
  (mixin (ebuild-interface<%>) ()
    (init-field [CRATES '()])
    (super-new)

    ;; Add "cargo" to inherited eclasses
    (ebuild-concat! inherits this "cargo")

    ;; append CRATES uris to the SRC_URIS
    (ebuild-concat! SRC_URI this
                    (src-uri #f "$(cargo_crate_uris ${CRATES})" #f))

    (define (unroll-CRATES)
      (string-append "CRATES=\"" "\n\t" (string-join CRATES "\n\t") "\""))

    (ebuild-concat! custom this unroll-CRATES)))


(define ebuild-cargo%
  (ebuild-cargo-mixin ebuild%))


#|
(displayln (new ebuild-cargo% [CRATES '("one" "two" "three")]))
|#
