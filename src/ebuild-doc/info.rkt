#lang info


(define pkg-desc "Library to ease ebuild creation. Documentation.")

(define version "16.1.0")

(define pkg-authors '(xgqt))

(define license 'GPL-2.0-or-later)

(define collection 'multi)

(define deps
  '("base"))

(define build-deps
  '("racket-doc"
    "net-doc"
    "scribble-lib"
    "ziptie-git"
    "ebuild-lib"
    "ebuild-templates"
    "ebuild-tools"
    "ebuild-transformers"))
