;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require scribble/example
          (for-label racket
                     ebuild
                     ebuild/transformers/src-uri))


@(define src-uri-eval
   (make-base-eval '(require
                     (only-in ebuild/ebuild src-uri->string)
                     ebuild/transformers/src-uri
                     )))


@title[#:tag "ebuild-transformers-src-uri"]{SRC_URI Transformers}

@defmodule[ebuild/transformers/src-uri]


@defproc[
 (url->src-uri
  [ur  path-string?]
  [pn  string?]
  )
 src-uri
 ]{
 Converts a URL @racket[ur] path-@racket[string] to a
 @racket[src-uri] struct.

 If @racket[pn] is supplied also a
 PN (package name) detection is carried out.

 @examples[
 #:eval src-uri-eval
 (define racket-url "https://mirror.racket-lang.org/installers/8.1/racket-8.1-src-builtpkgs.tgz")
 (define racket-src-uri (url->src-uri racket-url "racket"))
 racket-src-uri
 (src-uri->string racket-src-uri)
 ]
}
