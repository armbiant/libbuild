;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require scribble/example
          (for-label racket
                     ebuild
                     ebuild/templates/elisp))


@(define doc-eval
   (make-base-eval
    '(require ebuild ebuild/templates/elisp)))


@title[#:tag "ebuild-templates-elisp"]{Emacs Lisp libraries}


@defmodule[ebuild/templates/elisp]


@defmixin[ebuild-elisp-mixin (ebuild%) ()]{}


@defclass[
 ebuild-elisp% ebuild% ()
 ]{
 Pre-made class extending @racket[ebuild%] for writing ebuilds using the
 @link["https://gitweb.gentoo.org/repo/gentoo.git/tree/eclass/elisp.eclass"]{
  elisp.eclass}.

 @defconstructor[
 ([BYTECOMPFLAGS (or/c #f string?) #f]
  [ELISP_REMOVE  (or/c #f string?) #f]
  [ELISP_TEXINFO (or/c #f string?) #f]
  [EMACSFLAGS    (or/c #f string?) #f]
  [EMACSMODULES  (or/c #f string?) #f]
  [NEED_EMACS    (or/c #f number?) #f]
  [SITEETC       (or/c #f string?) #f]
  [SITEFILE      (or/c #f string?) "50${PN}-gentoo.el"]
  [SITELISP      (or/c #f string?) #f])
 ]{}

 @examples[
 #:eval doc-eval
 (define my-ebuild
   (new ebuild-elisp%
        [NEED_EMACS 25.1]
        [ELISP_REMOVE "${PN}-autoloads.el"]
        [ELISP_TEXINFO "${PN}.texi"]))
 (display my-ebuild)
 ]
}


@defmixin[package-elisp-mixin (package%) ()]{}


@defclass[
 package-elisp% ebuild% ()
 ]{
 Pre-made class extending @racket[package%] for writing ebuilds using the
 @link["https://gitweb.gentoo.org/repo/gentoo.git/tree/eclass/elisp.eclass"]{
  elisp.eclass}.

 @defconstructor[
 ([sitefile-name string? (format "50~a-gentoo.el" PN)]
  [sitefile-lisp (listof string?) '("(add-to-list 'load-path \"@SITELISP@\")")])
 ]{}

 @examples[
 #:eval doc-eval
 (define my-package
   (new package-elisp%
        [CATEGORY "app-emacs"]
        [PN "my-lib"]
        [ebuilds (hash (live-version) (new ebuild-elisp% [NEED_EMACS 25.1]))]))
 (send my-package show)
 ]
}
