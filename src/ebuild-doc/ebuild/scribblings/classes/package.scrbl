;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require scribble/example
          (for-label racket
                     racket/class
                     ebuild))


@(define package-eval
   (make-base-eval '(require racket/class ebuild/package)))


@title[#:tag "ebuild-classes-package"]{Package Class}


@defmodule[ebuild/package]

@defclass[
 package% object% ()
 ]{
 Package class.

 For creating packages.
 @defconstructor[
 (
  [CATEGORY  string?  "app-misc"]
  [PN        string?  "unknown"]
  [ebuilds   (hash/c package-version? (is-a?/c ebuild%))
             (hash (live-version) (new ebuild%))]
  [metadata  (is-a?/c metadata%)
             (new metadata%)]
  )
 ]{
  By default if ebuilds are not given the default @racket[ebuild%] object
  (with PN set to "unknown" is used) and
  if metadata is not given default "empty" @racket[metadata%] object is used.
 }

 @defmethod[
 (get-versions)
 (listof package-version?)
 ]{
  Return a @racket[list] of @racket[package-version]s
  extracted from @racket[ebuilds].

  @examples[
  #:eval package-eval
  (send (new package%) get-versions)
  ]
 }

 @defmethod[
 (get-PVs)
 (listof string?)
 ]{
  Return a @racket[list] of @racket[package-version]s as @racket[string]s
  extracted from @racket[ebuilds].

  @examples[
  #:eval package-eval
  (send (new package%) get-PVs)
  ]
 }

 @defmethod[
 (get-CATEGORY/PN)
 string?
 ]{
  Return a @racket[string] composed of: @racket[CATEGORY], @racket["/"]
  and @racket[PN].

  @examples[
  #:eval package-eval
  (send (new package%) get-CATEGORY/PN)
  ]
 }

 @defmethod[
 (get-Ps)
 (listof string?)
 ]{
  Return a @racket[list] of @racket[package-version]s
  joined with @racket[get-CATEGORY/PN].

  @examples[
  #:eval package-eval
  (send
   (new package%
        [CATEGORY  "dev-scheme"]
        [PN  "bytes"]
        [ebuilds
         (hash (simple-version "1") (new ebuild%)
               (simple-version "2") (new ebuild%))])
   get-Ps
   )
  ]
 }

 @defmethod[
 (show)
 void
 ]{
  Display @racket[ebuilds] and @racket[metadata].

  @examples[
  #:eval package-eval
  (send (new package%) show)
  ]
 }

 @defmethod[
 (save
  [pth path-string? (current-directory)]
  )
 void
 ]{
  Creates (uses their save methods) @racket[ebuilds]
  and @racket[metadata] of this package.
  The names of ebuilds are equal to so-called P ebuild variable
  which is composed of PN (taken from this @racket[package%] object)
  and PV (version, taken from the ebuilds hash).
  So ebuilds will be saved as @filepath{P.ebuild}.
 }
}
