;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require ziptie/git/hash
          ebuild/version)


@title[#:tag "ebuild"]{Racket-Ebuild}

@author[@author+email["Maciej Barć" "xgqt@riseup.net"]]


Library to ease ebuild creation and maintenance.

@(define upstream-tree
   "https://gitlab.com/gentoo-racket/racket-ebuild/-/tree/")

Version: @link[@(string-append upstream-tree @VERSION)]{@VERSION},
commit hash:
@(let ([git-hash (git-get-hash #:short? #t)])
   (case git-hash
     [("N/A")
      (displayln "[WARNING] Not inside a git repository!")
      git-hash]
     [else
      (link (string-append upstream-tree git-hash) git-hash)]))


@(define racket-ebuild-logo-src
   "./scribblings/assets/img/racket-ebuild-logo.svg")

@(if (file-exists? racket-ebuild-logo-src)
     (image racket-ebuild-logo-src)
     (printf "[WARNING] No ~a file found!~%" racket-ebuild-logo-src))


@table-of-contents[]


@include-section{about.scrbl}
@include-section{classes.scrbl}
@include-section{exported.scrbl}
@include-section{transformers.scrbl}
@include-section{templates.scrbl}
@include-section{tools.scrbl}
@include-section{external.scrbl}


@index-section[]
