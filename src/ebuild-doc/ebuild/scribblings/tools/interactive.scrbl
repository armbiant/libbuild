;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require "common-options.rkt"
          (for-label racket
                     ebuild/tools/interactive-exe))


@title[#:tag "ebuild-tools-interactive"]{Interactive}


@defmodule[ebuild/tools/interactive-exe]


@section[#:tag "ebuild-tools-interactive-about"]{About}

Interactively creates packages.


@section[#:tag "ebuild-tools-interactive-cli"]{Console usage}

@itemlist[
 @help-flag
 @version-flag
 ]
