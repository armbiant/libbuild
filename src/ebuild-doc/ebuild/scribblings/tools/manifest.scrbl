;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require "common-options.rkt"
          (for-label racket
                     ebuild/tools/manifest-exe))


@title[#:tag "ebuild-tools-manifest"]{Manifest}


@defmodule[ebuild/tools/manifest-exe]


@section[#:tag "ebuild-tools-manifest-about"]{About}

Regenerate Manifest files in given directories.

This program uses the @exec{ebuild} command (that is included in portage).


@section[#:tag "ebuild-tools-manifest-cli"]{Console usage}

@itemlist[
 @item{
  @Flag{f} or @DFlag{force}
  --- force regeneration of distfiles
 }

 @help-flag
 @version-flag
 ]
