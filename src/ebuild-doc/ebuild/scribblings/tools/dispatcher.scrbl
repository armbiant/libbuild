;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require "common-options.rkt"
          (for-label racket
                     ebuild/tools/dispatcher-exe))


@title[#:tag "ebuild-tools-dispatcher"]{Dispatcher (racket-ebuild)}


@defmodule[ebuild/tools/dispatcher-exe]


@section[#:tag "ebuild-tools-dispatcher-about"]{About}

Invoked from command-line as @exec{racket-ebuild}.

Dispatcher calls a Racket-Ebuild sub-command given as 1st argument with
rest leftover arguments, similar to how @exec{git} calls it's sub-commands.

For example:
when calling @exec{racket-ebuild commit -s}
the dispatcher will execute @exec{racket-ebuild-commit -s}.


@section[#:tag "ebuild-tools-dispatcher-cli"]{Console usage}

@itemlist[
 @item{
  @Flag{l} or @DFlag{list}
  --- list available subcommands
 }

 @help-flag
 @version-flag
 ]
