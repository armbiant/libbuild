;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (only-in scribble/bnf nonterm)
          "common-options.rkt"
          (for-label racket
                     ebuild/tools/commit-exe))


@title[#:tag "ebuild-tools-commit"]{Commit}


@defmodule[ebuild/tools/commit-exe]


@section[#:tag "ebuild-tools-commit-about"]{About}

Create a commit.

This utility is inspired by @exec{repoman commit}
and new utility meant to take place of @exec{repoman} - @exec{pkgdev commit}.


@section[#:tag "ebuild-tools-commit-cli"]{Console usage}

@itemlist[
 @item{
  @Flag{-m} @nonterm{commit-message} or
  @DFlag{--message} @nonterm{commit-message}
  --- specify the main commit message
 }
 @item{
  @Flag{-M} @nonterm{commit-message} or
  @DFlag{--aux-message} @nonterm{commit-message}
  --- specify the auxiliary commit message,
  auto-line-wrapped
 }

 @item{
  @Flag{-b} @nonterm{gentoo-bug/url} or
  @DFlag{--bug} @nonterm{gentoo-bug/url}
  --- add "Bug" tag for a given Gentoo or upstream bug/URL
 }
 @item{
  @Flag{-c} @nonterm{gentoo-bug/url} or
  @DFlag{--closes} @nonterm{gentoo-bug/url}
  --- add "Closes" tag for a given Gentoo or upstream bug/URL
 }

 @item{
  @Flag{-d} or @DFlag{--dry-run}
  --- do not make commits
 }
 @item{
  @Flag{-e} or @DFlag{--edit}
  --- force editing the commit even if message is not empty
 }
 @item{
  @Flag{-E} or @DFlag{--editor}
  --- overwrite the EDITOR environment variable,
  EDITOR is used to edit the commit message when @DFlag{--edit}
  flag is enabled
 }

 @item{
  @Flag{-a} or @DFlag{--all}
  --- stage all changed/new/removed files
 }
 @item{
  @Flag{-u} or @DFlag{--update}
  --- stage all changed files
 }

 @item{
  @Flag{-T} or @DFlag{--test-build}
  --- test modified, staged ebuilds by executing their test phases,
  produces a lot of noise
  and requires portage user privileges and that the user is in portage group
 }
 @item{
  @Flag{-s} or @DFlag{--scan}
  --- scan using "pkgcheck"
 }
 @item{
  @Flag{-n} or @DFlag{--scan-nonfatal}
  --- do not fail when scanning with "pkgcheck"
 }
 @item{
  @Flag{-k} or @DFlag{--ask-continue}
  --- ask whether user wants to continue after failed "pkgcheck" scan,
  only effective if both @DFlag{--scan} and @DFlag{--scan-nonfatal}
  flags are enabled
 }

 @item{
  @Flag{-U} or @DFlag{--update-manifests}
  --- update Manifest files
 }
 @item{
  @Flag{-S} or @DFlag{--sign}
  --- sign the created commit
 }

 @item{
  @Flag{-B} @nonterm{package-version} or
  @DFlag{--bump} @nonterm{package-version}
  --- set the commit message to "bump to @nonterm{package-version}"
  (use when updating ebuilds)
 }
 @item{
  @DFlag{--bump-copy}
  --- if doing a package bump, copy the latest ebuild as that version,
  works only in conjunction with the @Flag{-B} / @DFlag{--bump} flag
 }

 @item{
  @Flag{-D} @nonterm{package-version} or
  @DFlag{--drop} @nonterm{package-version}
  --- set the commit message to "drop old @nonterm{package-version}"
  (use when removing ebuilds)
 }
 @item{
  @DFlag{--drop-remove}
  --- if doing a package drop, remove the specified ebuild version,
  works only in conjunction with the @Flag{-D} / @DFlag{--drop} flag
 }

 @help-flag
 @version-flag
 ]
