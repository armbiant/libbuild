;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual


@title[#:tag "ebuild-external"]{External Resources}


@itemlist[
 @item{General
  @itemlist[@item{@link["https://gentoo.org/"]{
                Gentoo project page}}
            @item{@link["https://wiki.gentoo.org/"]{
                Gentoo Wiki}}
            @item{@link["https://devmanual.gentoo.org/"]{
                Gentoo Development Manual}}
            ]}
 @item{Ebuilds
  @itemlist[@item{@link["https://devmanual.gentoo.org/ebuild-writing/"]{
                Ebuild documentation}}
            @item{@link["https://devmanual.gentoo.org/ebuild-writing/functions/"]{
                Ebuild Functions}}
            @item{@link["https://wiki.gentoo.org/wiki/Ebuild_repository"]{
                Ebuild Repository documentation}}
            @item{@link["https://wiki.gentoo.org/wiki/Repository_format"]{
                Ebuild Repository Format}}
            @item{Selected Ebuild Repositoriess
               @itemlist[@item{@link["https://gitweb.gentoo.org/repo/gentoo.git"]{
                                          Official Gentoo ebuild repository (Gentoo's GitWeb)}}
                         @item{@link["https://github.com/gentoo/gentoo"]{
                                          Official Gentoo ebuild repository (GitHub)}}
                         @item{@link["https://gitlab.com/src_prepare/racket/racket-overlay"]{
                                          Experimental Racket Gentoo Overlay}}
                         ]}
            ]}
 @item{Metadata
  @itemlist[@item{@link["https://devmanual.gentoo.org/ebuild-writing/misc-files/metadata/"]{
                Package metadata documentation}}
            @item{@link["https://gentoo.org/dtd/metadata.dtd"]{
                Gentoo XML DTD of package metadata}}
            @item{@link["https://docs.racket-lang.org/xml/"]{
                Racket XML documentation}}
            ]}
 @item{Racket
  @itemlist[@item{@link["https://docs.racket-lang.org/guide/classes.html"]{
                Classes}}
            @item{@link["https://docs.racket-lang.org/xml/"]{
                XML}}
            ]}
 ]
