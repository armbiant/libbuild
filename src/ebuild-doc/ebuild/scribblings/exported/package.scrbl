;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require scribble/example
          (for-label racket
                     ebuild))


@(define package-eval
   (make-base-eval '(require ebuild/package)))


@declare-exporting[ebuild/package]


@title[#:tag "ebuild-exported-package"]{Package Functions}


@section{Package Version}

@defproc[
 (release?
  [v  any/c]
  )
 boolean?
 ]{
 Checks if given argement passes package-version validation.
 True if is a string a and has only digits
 and dots (".")
 and no more than one letter at the end.

 @examples[
 #:eval package-eval
 (release? "")
 (release? "0")
 (release? "1q")
 (release? "1.2.3a")
 (release? "1.2.3abc")
 ]
}

@defstruct[
 package-version
 (
  [release   release?]
  [phase     (or/c #f 'a 'alpha 'b 'beta)]
  [pre       (or/c boolean? exact-nonnegative-integer?)]
  [rc        (or/c boolean? exact-nonnegative-integer?)]
  [patch     (or/c #f exact-positive-integer?)]
  [revision  (or/c #f exact-positive-integer?)]
  )
 ]{

 @examples[
 #:eval package-eval
 (package-version "0a" 'beta 1 2 3 4)
 ]
}

@defproc[
 (live-version
  [nines  (and/c positive? exact-integer?)  4]
  )
 package-version
 ]{
 Generates a live @racket[package-version], that is: the one that has
 only @racket[9]s in "release" (all other fields are @racket[false]).
 Optional number determines athe number of @racket[9]s.

 @examples[
 #:eval package-eval
 (live-version 8)
 ]
}

@defproc[
 (simple-version
  [rel  release?]
  )
 package-version
 ]{
 Generates a @racket[package-version] with only a given @racket[rel]
 (other fields are #f).

 @examples[
 #:eval package-eval
 (simple-version "8.0")
 ]
}

@defproc[
 (package-version->string
  [ver  package-version?]
  )
 string?
 ]{
 Converts @racket[package-version] struct to a string.

 @examples[
 #:eval package-eval
 (package-version->string (package-version "0a" 'beta 1 2 3 4))
 (package-version->string (live-version))
 (package-version->string (simple-version "0a"))
 ]
}
