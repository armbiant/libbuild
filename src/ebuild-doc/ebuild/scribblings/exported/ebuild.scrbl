;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require scribble/example
          (for-label racket
                     ebuild))


@(define ebuild-eval
   (make-base-eval '(require ebuild/ebuild)))


@declare-exporting[ebuild/ebuild]


@title[#:tag "ebuild-exported-ebuild"]{Ebuild Functions}


@section{Constraint Flag}

@defstruct[
 cflag
 (
  [predicate  (or/c #f string?)]
  [content    (listof (or/c cflag? string?))]
  )
 ]{
 No restrictive contracts are enforced upon neither @racket[predicate]
 nor @racket[content] for now.

 @racket[cflag]s can be nested, for recursively transforming them,
 see @racket[cflag->string].

 @racket[predicate] symbolizes the logical ebuild condition to satisfy.

 @examples[
 #:eval ebuild-eval
 (define restrict
   (cflag "||" (list "qt5" "gtk")))
 restrict
 (cflag->string restrict)
 (display (cflag->string restrict))
 (define iuse
   (cflag "X?" (list (cflag "gtk?" (list "x11-libs/gtk+:3"))
                     (cflag "qt5?" (list "dev-qt/qtcore:5")))))
 iuse
 (for ([i '(0 1 2)])
   {define s (cflag->string iuse i)}
   (printf "~v~%" s)
   (displayln s))
 ]
}

@defproc[
 (cflag->string
  [fl    cflag?]
  [tabs  exact-nonnegative-integer? 1]
  [#:flat? flat? boolean? #f]
  )
 string?
 ]{
 Extracts @racket[cflag] contents to a @racket[string].

 @racket[cflag->string] calls itself recursively if a @racket[cflag]
 is encountered inside currently given @racket[fl] variable.

 Each internal recursive call increments @racket[tabs] by 1.
 If @racket[tabs] is given (grater than 0), then that tab step is applied
 to the result.

 If @racket[flat?] is @racket[true], then the produced @racket[string]
 does not contain neither tabs nor newlines.

 @examples[
 #:eval ebuild-eval
 (cflag->string (cflag "||" (list "qt5" "gtk")) #:flat? #t)
 ]
}


@section{Source URL}

Functions and structs used to create
@link["https://devmanual.gentoo.org/quickstart/#information-variables"
      "SRC_URI"]
ebuild variable.

@defstruct[
 src-uri
 (
  [flag  (or/c #f string?)]
  [url   string?]
  [name  (or/c #f string?)]
  )
 ]{
 Struct to hold URLs of SRC_URI.

 Fields represent inside a ebuild:
 @itemlist[@item{flag --- condition to grab given URL}
           @item{url --- any URL, maybe with shell variables inside}
           @item{name --- optional file name to which source downloaded from
             @racket[url] will be renamd to, ie.: @racket["${P}.tag.gz"]}
           ]
}

@defproc[
 (src-uri-src
  [su  src-uri]
  )
 string?
 ]{
 Creates a source string from given @racket[src-uri].

 @examples[
 #:eval ebuild-eval
 (src-uri-src
  (src-uri
   "doc" "https://mywebsite.com/${PN}-docs-${PV}.zip" "${P}-docs.zip"))
 ]
}

@defproc[
 (src-uri->string
  [su  src-uri]
  )
 string?
 ]{
 Creates a source string, with optional flag, from given @racket[src-uri].

 @examples[
 #:eval ebuild-eval
 (src-uri->string
  (src-uri
   "doc" "https://mywebsite.com/${PN}-docs-${PV}.zip" "${P}-docs.zip"))
 ]
}


@section{Shell Variables}

@defproc[
 (list-as-variable
  [name  string?]
  [lst   (or/c #f (listof (or/c number? string? symbol?)))]
  )
 (or/c #f string?)
 ]{
 Returns a string that can be used in creating POSIX-like shell scripts.
 @racket["name=\"value\""] if only value in @racket[list lst] is a string
 or 2 or more of any type of values
 or @racket["name=value"] if given @racket[list lst] with one value that
 is a number or symbol.
 If @racket[#f] is given as @racket[lst], then @racket[#f] is returned.

 @examples[
 #:eval ebuild-eval
 (list-as-variable "TURN_ON_FEATURE" '(YES))
 (list-as-variable "TURN_ON_FEATURES" '(THIS THAT))
 ]
}

@defproc[
 (as-variable
  [name   string?]
  [value  (or/c #f (or/c number? string? symbol?))] ...
  )
 (or/c #f string?)
 ]{
 Wrapper for @racket[list-as-variable].

 @examples[
 #:eval ebuild-eval
 (as-variable "TURN_ON_FEATURE" 'YES)
 ]
}

@defform[
 (make-variable name)
 #:contracts ([name (or/c identifier? string? symbol?)])
 ]{
 Uses @racket[as-variable].
 If @racket[name] is an identifier, then it is the name of variable
 and the value is what that @racket[name] resolves to.
 If @racket[name] is a string or a symbol then it is passed
 to both name and value of @racket[as-variable]
 (symbol is converted to a string).

 @examples[
 #:eval ebuild-eval
 (make-variable "this_variable_will_probably_change")
 (define Z "Zzz...")
 (make-variable Z)
 ]
}

@defproc[
 (list-as-array-variable
  [name  string?]
  [lst   (or/c #f (listof (or/c number? string? symbol?)))]
  )
 (or/c #f string?)
 ]{
 Simialr to @racket[list-as-variable], except for arrays.

 @examples[
 #:eval ebuild-eval
 (list-as-array-variable "FEATURES" '("mirror" "test"))
 ]
}

@defproc[
 (as-array-variable
  [name   string?]
  [value  (or/c #f (or/c number? string? symbol?))] ...
  )
 (or/c #f string?)
 ]{
 Wrapper for @racket[list-as-variable].

 @examples[
 #:eval ebuild-eval
 (as-array-variable "FEATURES" "mirror" "test")
 ]
}

@defform[
 (make-array-variable name)
 #:contracts ([name (or/c identifier? string? symbol?)])
 ]{
 Uses @racket[list-as-array-variable] and @racket[as-array-variable].

 @examples[
 #:eval ebuild-eval
 (make-array-variable "this_variable_will_probably_change")
 (define FEATURES '("mirror" "test"))
 (make-array-variable FEATURES)
 ]
}


@section{Object manipulation}

@defform[
 (ebuild-append! id obj lst)
 #:contracts ([id identifier?] [obj (is-a? ebuild%)] [lst list?])
 ]{
  Calls @method[ebuild% append!] method
  of a given @racket[obj] @racket[ebuild%] object.
}

@defform[
 (ebuild-concat! id obj v)
 #:contracts ([id identifier?] [obj (is-a? ebuild%)] [v any/c])
 ]{
  Calls @method[ebuild% concat!] method
  of a given @racket[obj] @racket[ebuild%] object.
}
