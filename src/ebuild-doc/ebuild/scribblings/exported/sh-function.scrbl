;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require scribble/example
          (for-label racket
                     ebuild))


@(define sh-function-eval
   (make-base-eval '(require ebuild/sh-function)))


@title[#:tag "ebuild-exported-sh-function"]{Shell Script Functions}


@defmodule[ebuild/sh-function]

@section{POSIX shell script function creation}

Ease the creation of ebuild scripts' "body".

@defstruct[
 sh-function
 (
  [name  string?]
  [body  string?]
  )
 ]{

 @examples[
 #:eval sh-function-eval
 (sh-function "my_function" "do_something || die")
 ]
}

@defproc[
 (sh-function->string
  [sf  sh-function?]
  )
 string?
 ]{
 Creates a string that looks like a function from as POSIX shell script.

 @examples[
 #:eval sh-function-eval
 (display (sh-function->string
           (sh-function "my_function" "do_something || die")))
 ]
}

@defproc[
 (unroll-sh-functions
  [lst  (listof sh-function?)]
  )
 string?
 ]{
 Uses @racket[sh-function->string] to create a shell script body string.
 Basically it is some POSIX shell script functions.
}

@defproc[
 (make-script
  [#:indent indent  exact-integer?]
  [strs    string?] ...
  )
 string?
 ]{
 Produces a shell script body from any amount of given strings,
 with the indentation @racket[indent] represented by tab characters
 (this its to comply with the ebuild format where we indent with tabs).

 This function exists to ease writing custom ebuild (shell script) functions.

 @examples[
 #:eval sh-function-eval
 (display (sh-function->string
           (sh-function "my_function" (make-script "do_something || die"))))
 ]
}
