#!/usr/bin/env racket


;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require racket/cmdline
         threading
         (prefix-in exn: racket/exn)
         (prefix-in system: (only-in racket/system system*))
         (prefix-in upi: upi/dirname)
         (prefix-in ebuild: ebuild/version))


(define program "racket-ebuild")


(define (subcommand-fail+message subcommand [reason #f])
  (printf "[ERROR] Subcommand ~a failed!~%" subcommand)
  (when reason
    (printf "Reason: ~a" reason))
  ;; Exit because we want to immediately stop.
  (exit 1))

(define (run-subcommand subcommand subcommand-args)
  (with-handlers ([exn:fail?
                   (lambda (e)
                     (subcommand-fail+message
                      subcommand (exn:exn->string e)))])
    (unless (apply system:system* subcommand subcommand-args)
      (subcommand-fail+message subcommand))))

(define (find-subcommands self-path)
  (let ([rx (regexp (string-append program ".*"))])
    (~>> self-path
         upi:dirname
         directory-list
         (filter (lambda (pth) (regexp-match rx pth))))))


(define (main)
  (define self-args
    (current-command-line-arguments))

  (define self-path
    (path->string (find-system-path 'run-file)))

  ;; NOTICE: with current implementation If there are any actual
  ;; racket-ebuild-help, racket-ebuild-version, racket-ebuild-list, etc.
  ;; binaries, then they will NEVER be picked!

  (cond
    [(or (= (vector-length self-args) 0)
         (regexp-match-exact? #rx"-.*" (vector-ref self-args 0)))
     (command-line
      #:program program

      #:ps
      ""
      "Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>"
      "Licensed under the GNU GPL v2 License"

      #:once-each
      [("-l" "--list")
       "List available subcommands"
       (for-each displayln (find-subcommands self-path))
       (exit 0)]
      [("-V" "--version")
       "Show the version of this program"
       (ebuild:print-version program)
       (exit 0)]

      ;; To display correctly the help message,
      ;; we do not take care of handling proper invocation here
      #:args (subcommand . subcommand-args)
      (void))]
    [(member (vector-ref self-args 0) '("help" "version"))
     (let ([switch (list (string-append "--" (vector-ref self-args 0)))])
       (if (= (vector-length self-args) 1)
           (run-subcommand self-path switch)
           (run-subcommand self-path (cons (vector-ref self-args 1) switch))))]
    [(member (vector-ref self-args 0) '("list"))
     (let ([switch (list (string-append "--" (vector-ref self-args 0)))])
       (run-subcommand self-path switch))]
    [else
     ;; The dispatcher:
     (let* ([args-lst   (vector->list self-args)]
            [subcommand (string-append self-path "-" (car args-lst))])
       (run-subcommand subcommand (cdr args-lst)))]))


(module+ main
  (main))
