#!/usr/bin/env racket


;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require racket/class
         racket/cmdline
         (prefix-in upi: upi/dirname)
         (prefix-in ebuild: ebuild/version)
         (prefix-in metadata: ebuild/metadata)
         (prefix-in modify-metadata: "private/modify-metadata.rkt"))


(define (main)
  (define program "modify-metadata")

  (define fields-to-remove
    (make-parameter '()))

  (define fields-to-add
    (make-parameter '()))

  (command-line
   #:program program

   #:ps
   ""
   "Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>"
   "Licensed under the GNU GPL v2 License"

   #:multi
   [("-a" "--add")
    name contents
    "Add a field of name with contents"
    (fields-to-add (append (fields-to-add)
                           `((,(string->symbol name)
                              ,@(regexp-split "\\|" contents)))))]
   [("-r" "--remove")
    name
    "Remove a field with name"
    (fields-to-remove (append (fields-to-remove)
                              (list (string->symbol name))))]

   #:once-each
   [("-V" "--version")
    "Show the version of this program"
    (ebuild:print-version program)
    (exit 0)]

   #:args files
   (for ([f files])
     (cond
       [(file-exists? f)
        (modify-metadata:modify-file f (fields-to-add) (fields-to-remove))]
       [else
        (let ([dir (upi:dirname f)])
          (printf "[WARNING] File at path ~a does not exist!~%" f)
          (printf "[WARNING] Creating a bare metadata file in \"~a\".~%" dir)
          (send (new metadata:metadata%) save dir))]))))


(module+ main
  (main))
