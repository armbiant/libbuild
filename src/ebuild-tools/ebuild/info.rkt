#lang info


(define racket-launcher-names
  '("racket-ebuild"
    "racket-ebuild-clean-versions"
    "racket-ebuild-commit"
    "racket-ebuild-fix-head"
    "racket-ebuild-githost2metadata"
    "racket-ebuild-interactive"
    "racket-ebuild-keywords"
    "racket-ebuild-manifest"
    "racket-ebuild-modify-metadata"
    "racket-ebuild-pkgname"
    "racket-ebuild-url2pkg"))

(define racket-launcher-libraries
  '("tools/dispatcher-exe.rkt"
    "tools/clean-versions-exe.rkt"
    "tools/commit-exe.rkt"
    "tools/fix-head-exe.rkt"
    "tools/githost2metadata-exe.rkt"
    "tools/interactive-exe.rkt"
    "tools/keywords-exe.rkt"
    "tools/manifest-exe.rkt"
    "tools/modify-metadata-exe.rkt"
    "tools/pkgname-exe.rkt"
    "tools/url2pkg-exe.rkt"))
