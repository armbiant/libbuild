#lang info


(define pkg-desc "Library to ease ebuild creation. Tools.")

(define version "16.1.0")

(define pkg-authors '(xgqt))

(define license 'GPL-2.0-or-later)

(define collection 'multi)

(define deps
  '("base"
    "typed-racket-lib"
    "threading-lib"
    "upi-lib"
    "ziptie-git"
    "ebuild-lib"
    "ebuild-templates"
    "ebuild-transformers"))

(define build-deps
  '())
