;; This file is part of racket-ebuild - library to ease ebuild creation.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require racket/class
         racket/port
         xml
         xml/path
         threading
         ebuild/metadata
         "private/metadata.rkt")

(provide (all-defined-out))


(define (xexpr->maintainers xexpr)
  (~>> xexpr
       (from-pkgmetadata 'maintainer)
       (map (lambda (x)
              (maintainer
               (string->symbol (se-path* '(maintainer #:type) x))
               ((lambda (v) (and (string? v) (string->symbol v)))
                (se-path* '(maintainer #:proxied) x))
               (se-path* '(maintainer email) x)
               (se-path* '(maintainer name)  x)
               (se-path* '(maintainer description) x))))))

(define (xexpr->longdescriptions xexpr)
  (~>> xexpr
       (from-pkgmetadata 'longdescription )
       (map (lambda (x)
              (longdescription
               (se-path* '(longdescription #:lang) x)
               (se-path* '(longdescription       ) x))))))

(define (xexpr->slots xexpr)
  (~>> xexpr
       (from-pkgmetadata 'slots)
       (map (lambda (x)
              (slots
               (se-path* '(slots #:lang) x)
               (map slot
                    (se-path*/list '(slots slot #:name) x)
                    (se-path*/list '(slots slot       ) x))
               (se-path* '(slots subslots) x))))))

(define (xexpr->stabilize-allarches xexpr)
  (~>> xexpr
       extract-pkgmetadata
       (member '(stabilize-allarches ()))
       (and _ #t)))

;; CONSIDER: handle "pkg" tag <pkg>cat/pkg</pkg>

(define (xexpr->uses xexpr)
  (~>> xexpr
       (from-pkgmetadata 'use)
       (map (lambda (x)
              (use
               (se-path* '(use #:lang) x)
               (map uflag
                    (se-path*/list '(use flag #:name) x)
                    (~>> x
                         (se-path*/list '(use))
                         (filter-element 'flag)
                         (map (lambda~>>
                               (list-tail _ 2)  ; '(flag ((name)) strs ...)
                               (map (lambda (v)
                                      (if (list? v) (list-ref v 2) v)
                                      ))
                               (apply string-append))))))))))

(define (xexpr->upstream xexpr)
  (let ([x
         (for/first ([v (extract-pkgmetadata xexpr)]
                     #:when (and (list? v) (eq? 'upstream (car v))))
           v)])
    (cond
      [x
       (upstream
        (for/list ([maintainer (se-path*/list '(upstream) x)]
                   #:when (equal? 'maintainer (car maintainer)))
          (let ([status
                 (let ([s (se-path* '(maintainer #:status) maintainer)])
                   (and s (string->symbol s)))]
                [email  (se-path* '(maintainer email) maintainer)]
                [name   (se-path* '(maintainer name)  maintainer)])
            (upstreammaintainer status email name)))
        (se-path* '(upstream changelog) x)
        (se-path* '(upstream doc)       x)
        (se-path* '(upstream bugs-to)   x)
        (map (lambda (ty tr) (remote-id (string->symbol ty) tr))
             (se-path*/list '(remote-id #:type) x)
             (se-path*/list '(remote-id)        x)))]
      [else
       #false])))


(define (xexpr->metadata xexpr)
  (new metadata%
       [maintainers         (xexpr->maintainers         xexpr)]
       [longdescriptions    (xexpr->longdescriptions    xexpr)]
       [slots               (xexpr->slots               xexpr)]
       [stabilize-allarches (xexpr->stabilize-allarches xexpr)]
       [uses                (xexpr->uses                xexpr)]
       [upstream            (xexpr->upstream            xexpr)]))

(define (read-metadata [port (current-input-port)])
  (~> port
      port->string
      clean-string
      (regexp-replace* " ?> ?" _ ">")
      (regexp-replace* " ?< ?" _ "<")
      string->xexpr
      xexpr->metadata))

(define (read-metadata-file pth)
  (read-metadata (open-input-file pth)))
