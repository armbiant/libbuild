MAKE        ?= make
RACKET      := racket
RACO        := $(RACKET) -l raco --
SCRIBBLE    := $(RACO) scribble
SH          := sh
RM          := rm
RMDIR       := $(RM) -r

PWD         ?= $(shell pwd)
BIN         := $(PWD)/bin
CACHE       := $(PWD)/.cache
DOCS        := $(PWD)/docs
PUBLIC      := $(PWD)/public
SCRIPTS     := $(PWD)/scripts
SRC         := $(PWD)/src
TESTS       := $(PWD)/tests

COLLECTOR2_INSTALL_FLAGS    := --auto --skip-installed
COLLECTOR2_SETUP_FLAGS      := --avoid-main --no-docs --tidy
COLLECTOR2_TEST_FLAGS       := --heartbeat --submodule test --table
COLLECTOR2_OVERLAY          := $(CACHE)/collector2/overlay
COLLECTOR2_RUN_PACKAGE      := ebuild-lib
COLLECTOR2_RUN_FLAGS        := --create --directory $(COLLECTOR2_OVERLAY) --only-package $(COLLECTOR2_RUN_PACKAGE)


.PHONY: all
all: compile


ebuild-make-%:
	$(MAKE) -C $(SRC) DEPS-FLAGS=" --no-pkg-deps " $(*)


.PHONY: clean-bin
clean-bin:
	if [ -d $(BIN) ] ; then $(RMDIR) $(BIN) ; fi

.PHONY: clean-ebuild
clean-ebuild: ebuild-make-clean

.PHONY: clean-public
clean-public:
	if [ -d $(PUBLIC) ] ; then $(RMDIR) $(PUBLIC) ; fi

.PHONY: clean-scripts
clean-scripts:
	if [ -d $(SCRIPTS)/compiled ] ; then $(RMDIR) $(SCRIPTS)/compiled ; fi

.PHONY: clean
clean: clean-bin clean-ebuild clean-public clean-scripts


.PHONY: compile-ebuild
compile-ebuild: ebuild-make-compile

# No .PHONY
scripts/compiled:
	$(RACO) make -v $(SCRIPTS)/*.rkt

.PHONY: compile-scripts
compile-scripts: scripts/compiled

.PHONY: recompile-scripts
recompile-scripts:
	$(MAKE) clean-scripts
	$(MAKE) compile-scripts

.PHONY: compile
compile: recompile-scripts compile-ebuild


.PHONY: install-ebuild
install-ebuild: ebuild-make-install

.PHONY: install
install: install-ebuild


.PHONY: setup-ebuild
setup-ebuild: ebuild-make-setup

.PHONY: setup
setup: setup-ebuild


.PHONY: test-ebuild
test-ebuild: ebuild-make-test


.PHONY: remove-ebuild
remove-ebuild: ebuild-make-remove

.PHONY: remove
remove: remove-ebuild


# No .PHONY
$(BIN): compile-scripts
	$(RACKET) $(SCRIPTS)/exes.rkt

.PHONY: bin
bin:
	$(MAKE) -B $(BIN)


# Do not include shellcheck target as a dependency
# No .PHONY
.cache:
	mkdir -p $(CACHE)

.PHONY: shellcheck
shellcheck:
	find $(PWD) -type f -name "*.sh" -exec shellcheck {} +

.PHONY: test-unit
test-unit: .cache
	TMPDIR=$(PWD)/.cache $(RACKET) $(TESTS)/test.rkt --unit

.PHONY: test-integration
test-integration: .cache
	TMPDIR=$(PWD)/.cache $(RACKET) $(TESTS)/test.rkt --integration

.PHONY: test-deps
test-deps:
	$(MAKE) -C $(SRC) setup

.PHONY: clean-collector-overlay
clean-collector-overlay:
	if [ -d $(COLLECTOR2_OVERLAY) ] ; then $(RMDIR) $(COLLECTOR2_OVERLAY) ; fi

.PHONY: test-collector2
test-collector2:
	$(MAKE) clean-collector-overlay
	$(RACO) pkg install $(COLLECTOR2_INSTALL_FLAGS) collector2
	$(RACO) setup $(COLLECTOR2_SETUP_FLAGS) --pkgs collector2-lib
	$(RACO) test $(COLLECTOR2_TEST_FLAGS) --package collector2-test
	$(RACKET) --lib collector2 -- $(COLLECTOR2_RUN_FLAGS)

.PHONY: test
test: test-unit test-integration

.PHONY: test-all
test-all: test-deps test test-collector2


# Regeneration

.PHONY: completion
completion:
	$(SH) $(SCRIPTS)/completion.sh

.PHONY: license-mapping
license-mapping:
	$(RACKET) $(SCRIPTS)/generate-license-mapping.rkt   \
		$(shell portageq get_repo_path / gentoo)/metadata/license-mapping.conf


.PHONY: doc-index
doc-index:
	$(MAKE) -C $(SRC) setup-pkg-ebuild-doc \
		SETUP-FLAGS="--doc-index --no-launcher --no-pkg-deps"

$(PUBLIC):
	mkdir -p $(PUBLIC)

.PHONY: docs-html
docs-html: $(PUBLIC)
	cd $(DOCS) && $(SCRIBBLE) ++main-xref-in --dest $(PWD) \
		--dest-name public --htmls --quiet $(DOCS)/scribblings/main.scrbl

.PHONY: docs-latex
docs-latex: $(PUBLIC)
	$(RACKET) $(SCRIPTS)/doc.rkt latex

.PHONY: docs-markdown
docs-markdown: $(PUBLIC)
	$(RACKET) $(SCRIPTS)/doc.rkt markdown

.PHONY: docs-pdf
docs-pdf: $(PUBLIC)
	$(RACKET) $(SCRIPTS)/doc.rkt pdf

.PHONY: docs-text
docs-text: $(PUBLIC)
	$(RACKET) $(SCRIPTS)/doc.rkt text

.PHONY: docs-all
docs-all: docs-html docs-latex docs-markdown docs-pdf docs-text

.PHONY: public
public:
	$(MAKE) -B docs-html docs-markdown docs-text
