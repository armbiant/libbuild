#!/bin/sh


# This file is part of racket-ebuild - library to ease ebuild creation.
# Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v2 License
# SPDX-License-Identifier: GPL-2.0-or-later

# racket-ebuild is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# racket-ebuild is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.


trap 'exit 128' INT
set -e
export PATH


myroot="$( realpath "$( dirname "${0}" )/../" )"

cd "${myroot}"


for command in $(racket-ebuild -l) ; do
    echo "Generating completions for ${command}..."

    ziptie-make-completion \
        -t zsh "${command}" > ./extras/completion/zsh/"_${command}"
done
